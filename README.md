# MADE_Restaurant_Project_2020

Для парсинга инстаграм-аккаунта необходимо установить pyInstagram:
1. git clone https://github.com/olegyurchik/pyInstagram.git
2. cd pyInstagram
3. pip install .

Для запуска обученной нейронной сети на классификацию блюд необходимо:
1. Установить все завсисимости из requirements.txt
2. Скачать датасет и веса для модели из файлика links.txt
3. Упаковать их в соответствующие папки или изменить пути в ноутбуках
